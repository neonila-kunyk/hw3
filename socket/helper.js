import { rooms } from "./index.js";
import { rating } from "./game.js";
import * as config from "./config.js";
import { texts } from "../data.js";

export const getRoomId = (roomName) => Object.keys(rooms).indexOf(roomName);

export const findUser = (users, id) => {
  for (let user of users) {
    if (user.id === id) return user;
  }
}

export const getUserAndRoomBySocket = id => {
  let currentUser;
  let roomName;
  for (let room in rooms) {
    currentUser = findUser(rooms[room], id);
    if(currentUser) {
      roomName = room;
      break;
    }
  }
  return {currentUser, roomName};
}

export const checkIsAllReady = roomName => {
  let isAllReady = true;
  for (let user of rooms[roomName]) {
    if (user.isReady === false) isAllReady = false;
  }
  return isAllReady;
}

export const checkIsAllFinished = roomName => {
  let isAllReady = true;
  for (let user of rooms[roomName]) {
    if (user.progress !== 100) isAllReady = false;
  }
  return isAllReady;
}

export const getRandom = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const filterVisibleRooms = () => {
  const filtered = {...rooms};
  for (let room in filtered) {
    if (filtered[room].length === config.MAXIMUM_USERS_FOR_ONE_ROOM 
        || checkIsAllReady(room)) {
          delete filtered[room];
    }     
  }
  return filtered;
}

export const resetUsers = roomName => {
  for (let user of rooms[roomName]) {
    user.progress = 0;
    user.position = 0;
    user.isReady = false;
  }
}

export const fillRatingAfterTimeout = roomName => {
  if (!rating[roomName]) rating[roomName] = [];
  for (let user of rooms[roomName]) {
    let player;
    for (let note of rating[roomName]) {
      if (note?.name === user.name) player = user;
    }
    if (!player) rating[roomName].push({ time: config.SECONDS_FOR_GAME, name: user.name, position: user.position});
  }
}

export const setTimer = (io, socket) => {
  const { roomName } = getUserAndRoomBySocket(socket.id);
  let counter = config.SECONDS_TIMER_BEFORE_START_GAME;
  io.to(getRoomId(roomName)).emit("TIMER_DECREASE", counter--);
  let timer = setInterval(() => {
    if (counter >= 0) io.to(getRoomId(roomName)).emit("TIMER_DECREASE", counter--);
    else {
      clearInterval(timer);
      timer = null;
      const textIndex = getRandom(0, texts.length - 1);
      io.to(getRoomId(roomName)).emit("RECEIVE_GAME_INFO", textIndex);
    }
  }, 1000);
}
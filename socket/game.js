import {rooms} from "./index.js";
import * as config from "./config.js";
import { 
  getRoomId, 
  findUser, 
  getUserAndRoomBySocket, 
  checkIsAllReady, 
  checkIsAllFinished, 
  getRandom, 
  resetUsers, 
  fillRatingAfterTimeout,
  setTimer 
} from "./helper.js";
import { filterVisibleRooms } from "./helper.js";

export const rating = {};

export const initGame = (io, socket) => {

  const changeState = roomName => {
    const currentUser = findUser(rooms[roomName], socket.id);
    currentUser.isReady = !currentUser.isReady;
    io.to(getRoomId(roomName)).emit("CHANGE_STATE_DONE", { isReady: currentUser.isReady, id: currentUser.id });
    if (checkIsAllReady(roomName)) {
      io.emit("UPDATE_ROOMS", filterVisibleRooms());
      io.to(getRoomId(roomName)).emit("TIMER_START");
      setTimer(io, socket);
    }
  }

  const receive_text = text => {
    const { roomName } = getUserAndRoomBySocket(socket.id);
    let counter = config.SECONDS_FOR_GAME;
    io.to(getRoomId(roomName)).emit("RACE_TIMER_DECREASE", counter--);
    if(!rooms[roomName].raceTimer) {
      rooms[roomName].raceTimer = setInterval(() => {
        rooms[roomName].time = config.SECONDS_FOR_GAME - counter;
        if (counter >= 0) io.to(getRoomId(roomName)).emit("RACE_TIMER_DECREASE", counter--);
        else {
          timeIsOut();
        }
      }, 1000);
    }

    const inputLetter = key => {
      const { currentUser, roomName } = getUserAndRoomBySocket(socket.id);
      if (text[currentUser.position] === key) {
        currentUser.progress = ++currentUser.position * 100 / text.length;
        let penult = false;
        let last = false;
        io.to(getRoomId(roomName)).emit("UPDATE_USERS", rooms[roomName]);
        if (currentUser.position === text.length) last = true;
        else if (currentUser.position === text.length - 1) penult = true;
        socket.emit("RIGHT_LETTER", {penult, last});
        if (last) {
          io.to(getRoomId(roomName)).emit("FINISH_RACE", socket.id);
          if (!rating[roomName]) rating[roomName] = [];
          rating[roomName].push({ time: rooms[roomName].time, name: currentUser.name, position: currentUser.position});
          if (checkIsAllFinished(roomName)) {
            timeIsOut();
          }
        }
      }
    }
    socket.on("INPUT_LETTER", inputLetter);
  }

  const timeIsOut = () => {
    const { roomName } = getUserAndRoomBySocket(socket.id);
    clearInterval(rooms[roomName].raceTimer);
    rooms[roomName].raceTimer = null;
    fillRatingAfterTimeout(roomName);
    io.to(getRoomId(roomName)).emit("END_GAME", rating[roomName]);
    delete rating[roomName];
    resetUsers(roomName);
    io.to(getRoomId(roomName)).emit("UPDATE_USERS", rooms[roomName]);
    for (let user of rooms[roomName]) {
      io.to(getRoomId(roomName)).emit("CHANGE_STATE_DONE", { isReady: user.isReady, id: user.id });
    }
  }

  socket.on("CHANGE_STATE", changeState);
  socket.on("RECEIVE_TEXT", receive_text);
}
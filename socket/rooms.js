import {rooms} from "./index.js";
import * as config from "./config.js";
import { getRoomId, findUser, checkIsAllReady, setTimer } from "./helper.js";
import { filterVisibleRooms } from "./helper.js";

export const initRooms = (io, socket, username) => {

  const addRoom = roomName => {
    if (rooms[roomName]) socket.emit("ROOMNAME_ALREADY_IN_USE");
    else if (!roomName) socket.emit("ROOMNAME_IS_EMPTY");
    else {
      rooms[roomName] = [];
      io.emit("ADD_ROOM_DONE", roomName, username);
    }
  }

  const joinRoom = roomName => {
    if (rooms[roomName].length === config.MAXIMUM_USERS_FOR_ONE_ROOM) socket.emit("TOO_MANY_USERS", roomName);
    else {
      rooms[roomName].push({ name: username, id: socket.id, isReady: false, progress: 0, position: 0});
      socket.join(getRoomId(roomName), () => {
        io.emit("UPDATE_ROOMS", filterVisibleRooms());
        io.to(socket.id).emit("JOIN_ROOM_DONE", roomName);
        io.to(getRoomId(roomName)).emit("UPDATE_USERS", rooms[roomName]);
      });
    }
  }

  const quitRoom = roomName => {
    const currentUser = findUser(rooms[roomName], socket.id);
    rooms[roomName].splice(rooms[roomName].indexOf(currentUser), 1);
    io.emit("UPDATE_ROOMS", filterVisibleRooms());
    socket.leave(getRoomId(roomName), () => {
      if (rooms[roomName].length === 0) delete rooms[roomName];
      else {
        io.to(getRoomId(roomName)).emit("UPDATE_USERS", rooms[roomName]);
        io.to(socket.id).emit("QUIT_ROOM_DONE");
        if (checkIsAllReady(roomName)) {
          io.to(getRoomId(roomName)).emit("TIMER_START", config.SECONDS_TIMER_BEFORE_START_GAME);
          setTimer(io, socket);
        }
      }
    });
  }

  socket.on("ADD_ROOM", addRoom);
  socket.on("JOIN_ROOM", joinRoom);
  socket.on("QUIT_ROOM", quitRoom);
}
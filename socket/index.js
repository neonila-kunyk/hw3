import { initRooms } from "./rooms";
import { initGame, rating } from "./game.js";
import { 
  getRoomId, 
  getUserAndRoomBySocket, 
  checkIsAllFinished, 
  resetUsers, 
  fillRatingAfterTimeout, 
  checkIsAllReady,
  setTimer} from "./helper.js";
import { filterVisibleRooms } from "./helper.js";
import * as config from "./config.js";

const users = new Set();
export const rooms = {};

export default io => {
  io.on("connection", socket => {
    socket.emit("UPDATE_ROOMS", filterVisibleRooms());

    const username = socket.handshake.query.username;
    if (users.has(username)) socket.emit("USERNAME_ALREADY_IN_USE");
    else users.add(username);

    initRooms(io, socket, username);
    initGame(io, socket);

    socket.on("disconnect", () => {
      users.delete(username);
      const { currentUser, roomName } = getUserAndRoomBySocket(socket.id);
      
      if (rooms[roomName]) {
        if (rooms[roomName].length <= 1) {
          clearInterval(rooms[roomName].raceTimer);
          rooms[roomName].raceTimer = null;
        }
        
        rooms[roomName].splice(rooms[roomName].indexOf(currentUser), 1);
        io.to(getRoomId(roomName)).emit("UPDATE_USERS", rooms[roomName]);
        io.emit("UPDATE_ROOMS", filterVisibleRooms());
        
        if (rating[roomName]) rating[roomName].splice(rating[roomName].indexOf(currentUser), 1);
        
        if (rooms[roomName].length === 0) {

          delete rooms[roomName];
          io.emit("UPDATE_ROOMS", filterVisibleRooms());

        } else if(checkIsAllFinished(roomName)) {

          fillRatingAfterTimeout(roomName);
          io.to(getRoomId(roomName)).emit("END_GAME", rating[roomName]);
          resetUsers(roomName);
          io.to(getRoomId(roomName)).emit("UPDATE_USERS", rooms[roomName]);
          io.emit("UPDATE_ROOMS", filterVisibleRooms());
          for (let user of rooms[roomName]) {
            io.to(getRoomId(roomName)).emit("CHANGE_STATE_DONE", { isReady: user.isReady, id: user.id });
          }

        } else if (checkIsAllReady(roomName)) {

          io.to(getRoomId(roomName)).emit("TIMER_START", config.SECONDS_TIMER_BEFORE_START_GAME);
          setTimer(io, socket);

        } 
        
        delete rating[roomName];
      }
    });
  });
};

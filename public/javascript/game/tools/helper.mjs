export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);
  if (className) addClass(element, className);
  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);

export const getRandom = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const sortByName = (a, b) => {
  if (a.name > b.name) return 1;
  if (a.name < b.name) return -1;
  return 0;
}

export const sortByTimeAndPosition = (a, b) => {
  if(a.time === b.time) {
    if (a.position > b.position) return -1;
    if (a.position < b.position) return 1;
  } else {
    if (a.time > b.time) return 1;
    if (a.time < b.time) return -1;
  }
  return 0;
}

export const formatRating = (note, index, rating) => {
  const username = [];
  username.push(note.name);
  for (let i = index + 1; i < rating.length; i++) {
    if(note.time === rating[i].time && note.position === rating[i].position) {
      username.push(rating[i].name);
      rating.splice(i, 1);
    }
  }
  return username;
}
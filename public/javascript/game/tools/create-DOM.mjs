import { createElement } from "./helper.mjs";
import { getRandom } from "./helper.mjs";

export const createRoomCard = (name, number_of_users, socket) => {
  const roomCard = createElement({
    tagName: "div",
    className: `room-card room-${getRandom(1,5)}`,
  });
  const connectedUsers = createElement({
    tagName: "span",
    className: "room-connected_users",
  });
  const roomName = createElement({
    tagName: "h2",
    className: "room-name",
  });
  const joinButton = createElement({
    tagName: "button",
    className: "no-select",
    attributes: { id: "join-btn" }
  });

  joinButton.addEventListener("click", () => {
    socket.emit("JOIN_ROOM", name);
  })

  connectedUsers.innerHTML = number_of_users === 1 ? "1 user connected" : `${number_of_users} users connected`;
  roomName.innerHTML = name;
  joinButton.innerHTML = "Join room";

  roomCard.append(connectedUsers, roomName, joinButton);
  return roomCard;
}

export const createUserCard = (user, currentUser) => {
  const userCard = createElement({
    tagName: "div",
    className: "user-card",
  });
  const userInfo = createElement({
    tagName: "div",
    className: "user-info",
  });
  const statusClass = user.isReady ? "ready-status-green" : "ready-status-red";
  const userStatus = createElement({
    tagName: "div",
    className: statusClass,
    attributes: { id: `status-of-${user.id}` }
  });
  const userName = createElement({
    tagName: "h2",
    className: "username",
  });
  const progressBar = createElement({
    tagName: "div",
    className: "user-progress-bar",
  });
  const finished = user.progress === 100 ? "finish" : "";
  const userProgress = createElement({
    tagName: "div",
    className: `user-progress ${user.name} id-${user.id} ${finished}`,
  });

  userName.innerHTML = currentUser ? `${user.name} (you)` : user.name;
  userProgress.style.width = `${user.progress}%`

  userInfo.append(userStatus, userName);
  progressBar.append(userProgress);
  userCard.append(userInfo, progressBar);
  return userCard;
}

export const createPlayerItem = (names, prevPlace) => {
  const item = createElement({
    tagName: "li"
  });
  for (let name of names) {
    const nameSpan = createElement({
      tagName: "span",
      className: "player-name",
      attributes: { id: `place-${prevPlace+1}` }
    });
    nameSpan.innerHTML = name;
    item.append(nameSpan);
  }
  return item;
}
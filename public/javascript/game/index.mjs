import { initRooms } from "./rooms.mjs";
import { initGame } from "./game.mjs";
import { initExceptions } from "./exceptions.mjs";

const username = sessionStorage.getItem("username");
const numberOfLoad = sessionStorage.getItem("number_of_load");

if (!username || username === 'null') window.location.replace("/login");
if (!numberOfLoad) sessionStorage.setItem("number_of_load", 1);
else sessionStorage.setItem("number_of_load", +numberOfLoad + 1);

const socket = io("", { query: { username } });

initExceptions(socket);
initRooms(socket, username);
initGame(socket);



 







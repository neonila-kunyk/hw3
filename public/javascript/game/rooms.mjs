import { createRoomCard } from "./tools/create-DOM.mjs";
import { addClass, removeClass } from "./tools/helper.mjs";

export const initRooms = (socket, currentUsername) => {
  
  const roomsContainer = document.querySelector(".rooms-container");

  document.querySelector("#add-room-btn").addEventListener("click", () => {
    const name = prompt("Enter room name, please:");
    socket.emit("ADD_ROOM", name);
  });

  const updateRooms = rooms => {
    roomsContainer.innerHTML = "";
    for (let room in rooms) {
      const roomCard = createRoomCard(room, rooms[room].length, socket);
      roomsContainer.append(roomCard);
    }
  };

  const addRoomDone = (roomName, username) => {
    const roomCard = createRoomCard(roomName, 1, socket);
    roomsContainer.append(roomCard);
    if (currentUsername === username) socket.emit("JOIN_ROOM", roomName);
  }

  const joinRoomDone = roomName => {
    addClass(document.querySelector("#rooms-page"), "display-none");
    removeClass(document.querySelector("#game-page"), "display-none");
    document.querySelector("#game-page .page-title").innerHTML = roomName;
    document.querySelector("#quit-room-btn").addEventListener("click", () => {
      socket.emit("QUIT_ROOM", roomName);
    });
    document.querySelector("#ready-btn").addEventListener("click", () => {
      socket.emit("CHANGE_STATE", roomName);
    });
  };

  const quitRoomDone = () => {
    addClass(document.querySelector("#game-page"), "display-none");
    removeClass(document.querySelector("#rooms-page"), "display-none");
  };
    
  socket.on("UPDATE_ROOMS", updateRooms);
  socket.on("ADD_ROOM_DONE", addRoomDone);
  socket.on("JOIN_ROOM_DONE", joinRoomDone);
  socket.on("QUIT_ROOM_DONE", quitRoomDone);
}
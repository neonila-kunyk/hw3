import { createUserCard, createPlayerItem } from "./tools/create-DOM.mjs";
import { addClass, removeClass, sortByName, sortByTimeAndPosition, formatRating } from "./tools/helper.mjs";

export const initGame = socket => {

  const usersContainer = document.querySelector(".users-container");
  const timerElem = document.querySelector("#timer");

  document.querySelector("#quit-results-btn").addEventListener("click", () => {
    addClass(document.querySelector(".popup"), "display-none");
    removeClass(document.querySelector("#ready-btn"), "display-none");
    removeClass(document.querySelector("#quit-room-btn"), "visible-none");
  })

  const updateUsers = users => {
    console.log(users)
    usersContainer.innerHTML = '';
    users.sort(sortByName);
    for (let user of users) {
      if (users && user.id === socket.id) {
        const userCard = createUserCard(user, true);
        usersContainer.append(userCard);
      } else {
        const userCard = createUserCard(user, false);
        usersContainer.append(userCard);
      }
    }
  };

  const changeStateDone = ({ isReady, id }) => {
    const button = document.querySelector("#ready-btn");
    const statusCircle = document.querySelector(`#status-of-${id}`);
    if(isReady) {
      if (socket.id === id) {
        button.innerHTML = "NOT READY";
        button.style.background = "radial-gradient(#fd8484, #f85656)";
      }
      addClass(statusCircle, "ready-status-green");
      removeClass(statusCircle, "ready-status-red");
    } else {
      if (socket.id === id) {
        button.innerHTML = "READY";
        button.style.background = "radial-gradient(#60efbc, #58d5c9)";
      }
      addClass(statusCircle, "ready-status-red");
      removeClass(statusCircle, "ready-status-green");
    }
  }

  const timerStart = () => {
    addClass(document.querySelector("#ready-btn"), "display-none");
    addClass(document.querySelector("#quit-room-btn"), "visible-none");
    removeClass(timerElem, "display-none");
  };

  const timerDecrease = counter => {
    timerElem.innerHTML = counter;
  };

  const inputHandler = e => {
    socket.emit("INPUT_LETTER", e.key);
  }

  const receiveGameInfo = textIndex => {
    fetch(`https://key-racing.herokuapp.com/game/texts/${textIndex}`)
      .then(res => res.json())
      .then(data => {
        const textContainer = document.querySelector("#text-container");
        const simpleTextContainer = document.querySelector("#text-container .simple");
        const underlinedTextContainer = document.querySelector("#text-container .underlined");
        const timeToEnd = document.querySelector("#time-to-end");
        addClass(timerElem, "display-none");
        removeClass(textContainer, "display-none");
        removeClass(timeToEnd, "display-none");
        simpleTextContainer.innerHTML = data.slice(1);
        underlinedTextContainer.innerHTML = data[0];
        document.addEventListener('keydown', inputHandler);
        socket.emit("RECEIVE_TEXT", data);
      })
      .catch(error => console.log(error))
  }

  const raceTimerDecrease = counter => {
    const timeToEnd = document.querySelector("#time-to-end");
    if (counter === 1) timeToEnd.innerHTML = `${counter--} second left`;
    else if (counter >= 0) timeToEnd.innerHTML = `${counter--} seconds left`;
  };

  const rightLetter = ({penult, last}) => {
    const simpleTextContainer = document.querySelector("#text-container .simple");
    const colorizedTextContainer = document.querySelector("#text-container .colorized");
    const underlinedTextContainer = document.querySelector("#text-container .underlined");
    colorizedTextContainer.innerHTML += underlinedTextContainer.innerHTML;
    if (!last) underlinedTextContainer.innerHTML = simpleTextContainer.innerHTML[0].replace(" ", "&nbsp");
    else underlinedTextContainer.innerHTML = "";
    if (!penult && !last) simpleTextContainer.innerHTML = simpleTextContainer.innerHTML.slice(1);
    else simpleTextContainer.innerHTML = "";
  }

  const finishRace = id => {
    const userProgress = document.querySelector(`.user-progress.id-${id}`);
    addClass(userProgress, "finish");
  }

  const endGame = rating => {
    const textContainer = document.querySelector("#text-container");
    const timeToEnd = document.querySelector("#time-to-end");
    addClass(textContainer, "display-none");
    addClass(timeToEnd, "display-none");
    const colorizedTextContainer = document.querySelector("#text-container .colorized");
    colorizedTextContainer.innerHTML = "";
    document.removeEventListener('keydown', inputHandler);
    rating = rating.sort(sortByTimeAndPosition).map(formatRating).filter(el => !!el);
    removeClass(document.querySelector(".popup"), "display-none");
    const items = rating.map(createPlayerItem);
    const list = document.querySelector(".popup-list");
    list.innerHTML = "";
    list.append(...items);
  }
    
  socket.on("UPDATE_USERS", updateUsers);
  socket.on("CHANGE_STATE_DONE", changeStateDone)
  socket.on("TIMER_START", timerStart);
  socket.on("TIMER_DECREASE", timerDecrease);
  socket.on("RECEIVE_GAME_INFO", receiveGameInfo);
  socket.on("RACE_TIMER_DECREASE", raceTimerDecrease);
  socket.on("RIGHT_LETTER", rightLetter);
  socket.on("FINISH_RACE", finishRace);
  socket.on("END_GAME", endGame);
}